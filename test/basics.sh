#!/bin/bash
# -*- coding: utf-8, tab-width: 2 -*-


function basics () {
  export LANG{,UAGE}=en_US.UTF-8  # make error messages search engine-friendly
  local SELFPATH="$(readlink -m "$BASH_SOURCE"/..)"
  cd "$SELFPATH" || return $?
  source ../fail_retry_delay_loop.sh --lib || return $?

  local TESTS=(
    test_grow_file
    )
  local TEST=
  for TEST in "${TESTS[@]}"; do
    echo "=== $TEST ==="
    "$TEST" || return $?$(echo "E: $TEST failed, rv=$?" >&2)
    echo
  done
  echo "=== All tests passed. ==="
}


function test_grow_file () {
  local CNT="tmp.counter.$$"
  >"$CNT" || return $?
  SECONDS=0
  RETRY_DELAY=2s fail_retry_delay_loop env:RETRY_DELAY \
    ./grow_file.sh "$CNT" 4 || return $?
  local DURA="$SECONDS"
  [ "$(cat -- "$CNT")" == .... ] || return 2$(echo "E: bad data" >&2)
  rm -- "$CNT"
  [ "$DURA" -ge 6 ] || return 2$(echo "E: finished too early" >&2)
  [ "$DURA" -le 8 ] || return 2$(echo "E: finished too late" >&2)
}










[ "$1" == --lib ] && return 0; basics "$@"; exit $?
